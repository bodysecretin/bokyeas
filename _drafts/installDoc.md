---
title: "GATSBY博客主题安装"
date: "2021-03-01"
description: ""
tags:
    - tags1
    - tags2
keywords:
    - keywords1
    - keywords2
draft: true
---



# 作者网站地址--https://github.com/junhobaik/junhobaik.github.io



## 英文文档安装使用说明书地址：https://github.com/junhobaik/junhobaik.github.io/wiki/Document-(Borderless)



## Gatsby Blog Starter（主题）“无边界”

使用Gatsby，
无边界博客主题创建的静态网站。

本文档基于到Github Pages的发行。

**[无边界的DEMO网站](https://junhobaik.github.io/)**

- 设计简单，色彩最少，无边框
- 创建Markdown帖子（Markdown +表情符号，ktex）
- 搜索引擎优化（SEO）
- 在Safari浏览器等帖子中支持阅读器模式
- Google Analytics（分析）支持
- Google Adsense支持
- Disqus评论功能



## 安装

有两种方法可以开始使用此博客。

1. 在仓库叉之后使用
2. 在存储库克隆后使用



### ！重要的

**不要在master分支上工作。**

如果您按照上述步骤进行操作，那么您正在处理的分支就在开发中。
开始在这个开发分支上工作。您可以修改develop分支，也可以与develop分支分开创建一个分支。

使用master分支进行部署，并找到生成的文件。
您可以在develop分支上工作，并使用deploy命令对其进行部署。
deploy命令自动部署到没有直接访问权限的master分支。

### 1.在仓库**叉**之后使用

- 在分叉存储库之后，

1. `Setting > Option - Repository name`
   将存储库重命名为“ **username** .github.io”
2. `Setting > Branches - Default branch`
   更改默认分支。选择**显影**，然后按更新按钮继续。
3. 克隆您的存储库。
4. `$ npm i` 安装软件包
5. `$ npm start` 启动本地开发环境检查URL“ localhost：8000”

### 2.在存储**库克隆**之后使用

```
$ git clone -b develop --single-branch https://github.com/junhobaik/junhobaik.github.io.git [SITE_DIRECTORY]
$ npm install
```

此过程将基于develop分支克隆存储库。
然后安装所需的软件包。

```
$ npm start
```

启动本地开发环境
检查URL“ localhost：8000”





## 基本设置

为您修改默认设置。

### 调整 `./_config.js`





## 撰写文章

帖子是用Markdown写的。

- 降价促销
- 表情符号支持
- ktex支持

您的帖子将位于该`./_posts`文件夹中。
您的草稿帖子将位于该`./_drafts`文件夹中。（在本地开发期间可以看到草稿，但在分布式Web上看不到）
。`_posts`文件夹和` _drafts`文件夹必须存在。**请勿同时删除两个文件夹**

文件的标题将成为该帖子的地址。如果输入日期（例如，以Jekyll的邮政文件标题格式），则该日期将从地址中排除。

前1.`./_posts/first-post.md`
地址：[https](https://junhobaik.github.io/first-post)：[//junhobaik.github.io/first-post](https://junhobaik.github.io/first-post)

1. `./_posts/2019-12-31-first-post.md`
   地址：[https](https://junhobaik.github.io/first-post) : [//junhobaik.github.io/first-post](https://junhobaik.github.io/first-post)

**用图片创建帖子**

将图像嵌入markdown内时，结构如下

创建地址为“”的帖子的示例`/first-post` 创建`./_posts/first-post`文件夹后，
Markdown文件的标题为`index.md`，
将图像文件放置在该文件夹中。

```
  _posts/
    first-post/
      index.md
      image-file-1.png
      image-file-2.jpg
      ...
```

### 系列文章

当您按如下方式在一个主题上撰写多个帖子时，可以使用系列功能来表明该帖子已包含在系列中。使用系列功能，您将在文章顶部看到一系列文章列表。

-创建博客，安装-创建博客，设置-创建博客，分发

如果您写这样的帖子，请按如下所示写文件名。

- `development-blog_1.md`
- `development-blog_2.md`
- `development-blog_3.md`

下划线（_）后面的数字是序列号。
underbar（_）之前的文件名必须相同。
文件名中只能有一个下划线（_）。
请勿在下划线（_）后混数字和字母。

### Markdown YAML前线

在Markdown帖子的顶部，输入该帖子的信息。`---`如下所示在两秒钟内输入信息。

```
---
title: title here...
date: 2018-01-01
tags:
  - javascript
  - ES6
keywords:
  - keyword1
  - keyword2
---

contents here...
```

| 多变的     | 描述                                                         |
| ---------- | ------------------------------------------------------------ |
| `title`    | **（必填）**输入帖子标题。如果包括特殊字符，请将其括在中`" "`。 |
| `date`     | **（必填）**输入此帖子的创建日期                             |
| `tags`     | **（可选）**输入帖子标签                                     |
| `keywords` | **（可选）** SEO元关键字                                     |

+所有值均未包含在`''`，中`""`。

#### 数据示例

```
# date Examples

## Case 1
date: 2018-09-01

## Case 2
date: 2018-09-01 22:00:00

## 'Time zone' Not Support
## There is no error but ignores the time zone
date: 2018-09-01 20:00:00 +0900
```

#### 标签和关键词

```
# tags & keywords Examples

## Case 1
tags: onlyOneTag

## Case 2
tags: [tag1, tag2]

## Case 3
tags:
  - tag1
  - tag2
```



## 部署

部署前检查清单

1. 确保您的Github存储库名为username.github.io。
2. 确保您已在中正确编写了siteUrl `./_config.js`。
3. 确保您正在使用的分支不是主分支。（有关说明，请参阅“安装”部分）

```
$ npm run deploy
```

部署从上述命令开始。提交将自动进行到master分支。这需要花费数十秒，有时甚至是几分钟的时间才能反映出来。

现在，您可以输入并验证您的地址。





***

gatsby 安装插件使用命令

$ npm install --save gatsby-plugin-sharp

gatsby-plugin-sharp 为安装插件名称

***



### 常见问题

## 解决方法

经过分析后发现是由于安装这些包时需要安装依赖包，而其中部分依赖包需要从GitHub上下载，而GitHub的资源库DNS有问题，导致这些依赖包无法安装而报错。

找到原因是由于Github DNS的问题。我们在本地hosts文件中(路径C:\Windows\System32\drivers\etc)添加以下内容(Github相关域名的解析地址)，然后用npm清楚所有缓存（清除命令为：>npm cache clean -f），之后再重新安装就成功了。

```
192.30.255.112	gist.github.com
192.30.255.112	github.com
192.30.255.112	www.github.com
151.101.56.133	avatars0.githubusercontent.com
151.101.56.133	avatars1.githubusercontent.com
151.101.56.133	avatars2.githubusercontent.com
151.101.56.133	avatars3.githubusercontent.com
151.101.56.133	avatars4.githubusercontent.com
151.101.56.133	avatars5.githubusercontent.com
151.101.56.133	avatars6.githubusercontent.com
151.101.56.133	avatars7.githubusercontent.com
151.101.56.133	avatars8.githubusercontent.com
151.101.56.133	camo.githubusercontent.com
151.101.56.133	cloud.githubusercontent.com
151.101.56.133	gist.githubusercontent.com
151.101.56.133	marketplace-screenshots.githubusercontent.com
151.101.56.133	raw.githubusercontent.com
151.101.56.133	repository-images.githubusercontent.com
151.101.56.133	user-images.githubusercontent.com
```

**特别强调：以上Github DNS内容可能会变动。如果修改后再次重装还是报错，可以在[github.com搜索最新的hosts文件](https://github.com/search?q=hosts)，尝试用不同的hosts文件内容，直到执行成功为止。**

更改hosts文件后记得刷新本地DNS。打开cmd，输入以下命令：

```
ipconfig /flushdns
```