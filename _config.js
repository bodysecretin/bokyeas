module.exports = {
  /** Site MetaData (Required all)*/
  /** 网站元数据（必填）*/
  /**网站标题 */
  title: `蒲野`,                       // (* Required-翻译“必填”)
  /** 网站描述*/
  description: `蒲野-`,          // (* Required-翻译“必填”)
  /** 作者名称 */
  author: `Author`,                         // (* Required-翻译“必填”)
  /** 语言设定 */
  language: 'ZH-cn',                        // (* Required-翻译“必填”) html lang, ex. 'en' | 'en-US' | 'ko' | 'ko-KR' | ...
  /**  */
  siteUrl: 'https://BOKYEA.COM',                      // (* Required-翻译“必填”)
    // ex.'https://junhobaik.github.io'
    // ex.'https://junhobaik.github.io/' << X, Do not enter "/" at the end.-翻译“不要在末尾输入“ /””

  /** Header */
  /** 网页页头 */
  /** 个人资料图片放置位置 */
  profileImageFileName: 'apple-touch-icon-3.png', // include filename extension ex.'profile.jpg'--包括文件扩展名，例如'profile.jpg'
    // The Profile image file is located at path "./images/" --个人资料图片文件位于路径“ ./images/,就是在src文件夹下的images”
    // If the file does not exist, it is replaced by a random image.--如果文件不存在，则会将其替换为随机图像。

  /** Home > Bio information*/
  comment: '蒲野 ',
  name: 'BokYea',
  company: '',
  //location: 'HongKong',
  email: '',
  website: '',                                                           // ex.'https://junhobaik.github.io'
  linkedin: '',                                                          // ex.'https://www.linkedin.com/in/junho-baik-16073a19ab'
  facebook: '',                                                          // ex.'https://www.facebook.com/zuck' or 'https://www.facebook.com/profile.php?id=000000000000000'
  instagram: '',                                                         // ex.'https://www.instagram.com/junhobaik'
  github: '',                                                            // ex.'https://github.com/junhobaik'

  /** Post */
  //** 是否使用目录 */
  enablePostOfContents: true,     // TableOfContents activation (Type of Value: Boolean. Not String --翻译内容“值的类型：布尔值（ture or false）。 不是字符串”)
  disqusShortname: '',            // comments (Disqus sort-name)
 //**社交分享图标激活 */
  enableSocialShare: true,        // Social share icon activation (Type of Value: Boolean. Not String)

  /** Optional */
  googleAnalytics: 'G-H3MHB3Q4X8',     // Google Analytics TrackingID. ex.'UA-123456789-0'
  googleSearchConsole: '', // content value in HTML tag of google search console ownership verification. ex.'w-K42k14_I4ApiQKuVPbCRVV-GxlrqWxYoqO94KMbKo'
  googleAdsenseSlot: '',   // Google Adsense Slot. ex.'5214956675'
  googleAdsenseClient: 'ca-pub-4977971660473600', // Google Adsense Client. ex.'ca-pub-5001380215831339'
    // Please correct the adsense client number(ex.5001380215831339) in the './static/ads.txt' file.
};
